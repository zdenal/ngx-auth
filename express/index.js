const app = require('./server.js');
const config = require('./config.js');

/*
 * Start server
 */
app.listen(config.PORT);
