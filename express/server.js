const express = require('express');
const app = express();
const router = express.Router();
const cors = require('cors');
const bodyParser = require('body-parser');

app.use(cors());
app.use(bodyParser.json());
/*
 * Add middleware. Because we defined the first parameter ( '/api' ), it will run
 * only for urls that starts with '/api/*'
 */
app.use('/api', require('./middlewares/auth.js'));
/*
 * Add the protected routes after '/api'
 */
app.use('/api', require('./controllers/user.js')(router));
app.use('/api', require('./controllers/message.js')(router));
/*
 * Add the '/hello-world' route handler
 */
app.use('/', require('./controllers/helloWorld.js')(router));
/*
 * Add the '/authenticate' route handler
 */
app.use('/', require('./controllers/auth.js')(router));

module.exports = app;
