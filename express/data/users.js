/**
 * Mocked database of users.
 * Rights are used on frontend only.
 */
module.exports = [
  {
    id: '1',
    username: 'cat',
    password: 'pass',
    rights: ['admin'],
    avatar: 'https://takiruna.files.wordpress.com/2010/02/gatto-con-gli-stivali.jpg'
  },
  {
    id: '2',
    username: 'donkey',
    password: 'pass',
    rights: ['manage'],
    avatar: 'https://avatarfiles.alphacoders.com/533/53313.jpg'
  },
  {
    id: '3',
    username: 'shrek',
    password: 'pass',
    rights: [],
    avatar: 'https://i1.sndcdn.com/avatars-000577573788-lry5u0-t500x500.jpg'
  }
];
