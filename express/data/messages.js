/**
 * Mocked database of messages.
 */
module.exports = [
  {
    id: '1',
    author: 'Donkey',
    text: 'Hey, come back there. I\'m not through with you yet.',
  },
  {
    id: '2',
    author: 'Shrek',
    text: 'Well, I\'m through with you.',
  },
  {
    id: '3',
    author: 'Donkey',
    text: 'Uh-uh. You know, with you it\'s always, "Me, me, me!" Wll, guess that! Now it\'s my turn! So you just shut up and pay attention! You\'re mean to me. You insult me and you don\'t appreciate anything that I do! You\'re always pushing me around or pushing me away.',
  },
  {
    id: '4',
    author: 'Shrek',
    text: 'Oh yeah? Well, if I treated you so bad, how come you came back?',
  },
  {
    id: '5',
    author: 'Donkey',
    text: 'Because that\'s what friends do! They forgive each other!',
  },
  {
    id: '6',
    author: 'Shrek',
    text: 'Oh yeah, you\'re right, Donkey. I forgive you... For stabbing me in the back!',
  },
];
