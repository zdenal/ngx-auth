# Express.js JWT secured app

## Installation

Edit `config.example.js` and save it as `config.js`

## Run it

```
npm run start-express
```

## Authorization

When you run the project, you should be able to load the URL `http://localhost:8080/hello-world`, but you shouldn't be able to access `http://localhost:8080/api/me`.

You can log in by sending a post on `http://localhost:8080/authenticate` and send there username and password. Full list of users including credentials is in `./data/users.js` file.

```
curl -XPOST -H "Content-Type: application/json" 'http://localhost:8080/authenticate' -d '{"username":"shrek","password":"pass"}'
```

You should get back something like:

```
{
  "jwt":"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjMiLCJ1c2VybmFtZSI6InNocmVrIiwiaWF0IjoxNTc5NjQzMTY0LCJleHAiOjE1Nzk2NDY3NjR9.jldktFA3dHVSplxc-LItDuIr2ruomaKvyLss3X_-y7E"
}
```

After decoding JWT payload, you will get basic information about the user:

```
{
  "id":"1",
  "username:"shrek"
}
```

Now, when you want to load `http://localhost:8080/api/me` and you send there `Authorization` header with JWT token from the previous response, you should be successful:

```
curl -XGET -H 'Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjMiLCJ1c2VybmFtZSI6InNocmVrIiwiaWF0IjoxNTc5NjQzMTY0LCJleHAiOjE1Nzk2NDY3NjR9.jldktFA3dHVSplxc-LItDuIr2ruomaKvyLss3X_-y7E' 'http://localhost:8080/api/me'
```
