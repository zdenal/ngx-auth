const jwt = require('jsonwebtoken');
const config = require('../config.js');
const users = require('../data/users.js');

module.exports = (req, res, next) => {
  /**
   * Check if authorization header is set.
   */
  if(req.hasOwnProperty('headers') && req.headers.hasOwnProperty('authorization')) {
    try {
      /**
       * Try to decode & verify the JWT token.
       * The token contains user's id (it can contain more informations)
       * and this is saved in req.user object.
       */
      req.user = jwt.verify(req.headers['authorization'], config.JWT_SECRET);
      const user = users.find(u => u.id === req.user.id);
      if (!user) {
        /**
         * If user is not found, return 401 status code with JSON error message.
         */
        return res.status(401).json({
          error: {
            message: `User with ID ${req.user.id} not found!`
          }
        });
      }
    } catch (err) {
      /**
       * If the authorization header is corrupted, it throws exception.
       * So return 401 status code with JSON error message.
       */
      return res.status(401).json({
        error: {
          message: 'Failed to authenticate token!'
        }
      });
    }
  } else {
    /**
     * If there is no autorization header, return 401 status code with JSON
     * error message.
     */
    return res.status(401).json({
      error: {
        message: 'No token!'
      }
    });
  }
  next();
  return;
};
