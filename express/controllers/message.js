const messages = require('../data/messages.js');

module.exports = router => {
  router.get('/messages', (req, res) => {
    res.json(messages);
  });

  return router;
};
