module.exports = router => {
  router.get('/hello-world', (req, res) => {
    res.json({ data: 'Hello World!' });
  });

  return router;
};
