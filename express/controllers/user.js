const users = require('../data/users.js');

module.exports = router => {
  router.get('/me', (req, res) => {
    // do not send password
    const { password, ...user } = users.find(u => u.id === req.user.id);
    res.json(user);
  });

  return router;
};
