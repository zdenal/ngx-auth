const jwt = require('jsonwebtoken');
const config = require('../config.js');
const users = require('../data/users.js');

module.exports = router => {
  router.post('/authenticate', (req, res) => {
    /*
     * Check if the username and password is correct.
     */
    const user = users.find(u => u.username === req.body.username);
    if (user && user.password === req.body.password) {
      res.json({
        jwt: jwt.sign(
          {
            id: user.id,
            username: user.username,
          },
          config.JWT_SECRET,
          { expiresIn: 60 * 60 }
        )
      });
    } else {
      /*
       * If the username or password was wrong, return 401 (Unauthorized)
       * status code and JSON error message.
       */
      res.status(401).json({
        error: {
          message: 'Wrong username or password!'
        }
      });
    }
  });

  return router;
};
