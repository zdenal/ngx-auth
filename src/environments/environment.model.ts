export interface Environment {
  production: boolean;
  apiUriPrefix: string;
}
