import { Environment } from './environment.model';

export const environment: Environment = {
  production: true,
  apiUriPrefix: 'http://localhost:8080'
};
