import { ActivatedRouteSnapshot, CanActivate, CanActivateChild, RouterStateSnapshot, UrlTree } from '@angular/router';
import { from, Observable, of } from 'rxjs';
import { concatMap, first } from 'rxjs/operators';

/**
 * Allows to chain more CanActivate or CanActivateChild guards and stop after first
 * result not being true. Works even for Observables and Promises.
 * See https://stackoverflow.com/questions/40589878/multiple-canactivate-guards-all-run-when-first-fails
 * for more information about similar issue.
 */
export function stopAfterFirstNotTrueCanActivateGuard(
  route: ActivatedRouteSnapshot,
  state: RouterStateSnapshot,
  guards: CanActivate[],
): Observable<boolean | UrlTree>;
export function stopAfterFirstNotTrueCanActivateGuard(
  route: ActivatedRouteSnapshot,
  state: RouterStateSnapshot,
  guards: CanActivateChild[],
  child: true,
): Observable<boolean | UrlTree>;
export function stopAfterFirstNotTrueCanActivateGuard(
  route: ActivatedRouteSnapshot,
  state: RouterStateSnapshot,
  guards: (CanActivate | CanActivateChild)[],
  child = false,
): Observable<boolean | UrlTree> {
  return from(guards || []).pipe(
    concatMap(guard => {
      const res = child ? (guard as CanActivateChild).canActivateChild(route, state) : (guard as CanActivate).canActivate(route, state);
      return canActivateResultAsObservable(res).pipe(first());
    }),
    first(res => res !== true, true)
  );
}

export function canActivateResultAsObservable(
  res: Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree
): Observable<boolean | UrlTree> {
  if (res instanceof Observable) {
    return res;
  } else if (res instanceof Promise) {
    return from(res);
  } else {
    return of(res);
  }
}
