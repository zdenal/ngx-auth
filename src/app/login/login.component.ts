import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import { AuthService } from '../auth/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  preserveWhitespaces: true,
})
export class LoginComponent implements OnInit {

  form: FormGroup;
  submitting = false;

  constructor(
    private auth: AuthService,
    private router: Router,
    private route: ActivatedRoute,
  ) { }

  ngOnInit() {
    this.buildForm();
  }

  login(): void {
    if (this.submitting || !this.form.valid) {
      return;
    }
    this.submitting = true;

    this.auth.authenticate$(this.form.value).subscribe(user => {
      console.log('User authenticated', user);
      this.router.navigateByUrl(this.route.snapshot.queryParamMap.get('redirect') || '/');
    }, err => {
      console.error('Authenticate error', err);
      this.submitting = false;
      let msg: string;
      if (err instanceof HttpErrorResponse && err.error && err.error.error && err.error.error.message) {
        msg = err.error.error.message;
      } else {
        msg = 'Sorry, we are not able to authenticate you';
      }
      alert(msg);
    });
  }

  private buildForm(): void {
    this.form = new FormGroup({
      username: new FormControl(null, Validators.required),
      password: new FormControl(null, Validators.required),
    });
  }

}
