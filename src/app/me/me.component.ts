import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { shareReplay } from 'rxjs/operators';

import { AuthUser } from '../auth/auth.model';
import { AuthService } from '../auth/auth.service';
import { Message } from '../messages/messages.model';
import { MessagesService } from '../messages/messages.service';

@Component({
  selector: 'app-me',
  templateUrl: './me.component.html',
  styleUrls: ['./me.component.css'],
  preserveWhitespaces: true,
})
export class MeComponent implements OnInit {

  messages$: Observable<Message[]>;
  user$: Observable<AuthUser>;

  constructor(
    private messagesService: MessagesService,
    private auth: AuthService,
  ) {
    this.messages$ = this.messagesService.getMessages$().pipe(shareReplay(1));
    this.user$ = this.auth.getUser$();
  }

  ngOnInit() {
  }

}
