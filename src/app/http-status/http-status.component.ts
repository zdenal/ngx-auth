import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-http-status',
  templateUrl: './http-status.component.html',
  styleUrls: ['./http-status.component.css']
})
export class HttpStatusComponent implements OnInit {

  status: number;

  constructor(route: ActivatedRoute) {
    this.status = route.snapshot.data.status;
  }

  ngOnInit() {
  }

}
