import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { environment } from '../../environments/environment';
import { Message } from './messages.model';

@Injectable({
  providedIn: 'root'
})
export class MessagesService {

  constructor(private http: HttpClient) { }

  getMessages$(): Observable<Message[]> {
    return this.http.get<Message[]>(`${environment.apiUriPrefix}/api/messages`);
  }

}
