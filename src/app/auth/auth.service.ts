import { Injectable } from '@angular/core';
import { LocalStorage, LocalStorageService } from 'ngx-webstorage';
import { Observable, of } from 'rxjs';
import { map, shareReplay, startWith, switchMap, tap } from 'rxjs/operators';

import { AuthApiService } from './auth-api.service';
import { AuthCredentials, AuthData, AuthUser } from './auth.model';

const dataKey = 'authData';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  @LocalStorage(dataKey)
  private data: AuthData;
  private data$: Observable<AuthData>;

  private user$: Observable<AuthUser>;

  constructor(
    private storage: LocalStorageService,
    private api: AuthApiService,
  ) {
    // Allow observe auth data
    this.data$ = (storage.observe(dataKey) as Observable<AuthData>).pipe(
      // storage.observe just emits new values
      startWith(this.data),
      // let's cache the value
      shareReplay(1),
    );

    // Grab user if authenticated
    this.user$ = this.data$.pipe(
      // Call the api
      switchMap(data => data ? this.api.getUser$(data) : of(null)),
      // and cache the result
      shareReplay(1),
    );
  }

  /**
   * Synchronous authenticated check.
   */
  isAuthenticated(): boolean {
    return !!this.data;
  }

  /**
   * Observable version of `isAuthenticated` method.
   */
  isAuthenticated$(): Observable<boolean> {
    return this.data$.pipe(map(authData => !!authData));
  }

  /**
   * Synchronous authenticated data.
   */
  getData(): AuthData {
    return this.data;
  }

  /**
   * Observable version of `getAuthData` method.
   */
  getData$(): Observable<AuthData> {
    return this.data$;
  }

  /**
   * Object with information about authenticated user.
   * It is asynchronous only, because it can contain more data
   * than synchronously stored authenticated data.
   */
  getUser$(): Observable<AuthUser> {
    return this.user$;
  }

  /**
   * Authenticate user using given credential and return authenticate data.
   * If credentials are wrong, error observable is returned.
   * @param credentials authenticate information
   */
  authenticate$(credentials: AuthCredentials): Observable<AuthData> {
    return this.api.authenticate$(credentials).pipe(
      tap(data => this.data = data),
    );
  }

  /**
   * Synchronous invalidation
   */
  invalidate(): void {
    this.storage.clear(dataKey);
  }

}
