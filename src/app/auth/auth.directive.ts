import { Directive, Input, OnDestroy, OnInit, TemplateRef, ViewContainerRef } from '@angular/core';
import { BehaviorSubject, combineLatest, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { AuthService } from './auth.service';

/**
 * Use this directive to show or hide content based on authentication status.
 * To show content use:
 *  <element *appAuth></element>
 * To hide content use:
 *  <element *appAuth="false"></element>
 */
@Directive({
  selector: '[appAuth]'
})
export class AuthDirective implements OnInit, OnDestroy {

  /**
   * Set to `false` for using negative condition.
   */
  @Input('appAuth') set value(value: boolean) {
    this.negative$.next(value === false);
  }

  private negative$ = new BehaviorSubject(false);
  private destroyed = new Subject();

  constructor(
    private templateRef: TemplateRef<any>,
    private viewContainer: ViewContainerRef,
    private auth: AuthService,
  ) { }

  ngOnInit(): void {
    combineLatest(
      this.auth.isAuthenticated$(),
      this.negative$,
    ).pipe(
      takeUntil(this.destroyed),
    ).subscribe(([authenticated, negative]) => this.updateView(negative ? !authenticated : authenticated));
  }

  ngOnDestroy() {
    this.destroyed.next();
  }

  protected updateView(visible: boolean): void {
    visible ? this.viewContainer.createEmbeddedView(this.templateRef) : this.viewContainer.clear();
  }

}
