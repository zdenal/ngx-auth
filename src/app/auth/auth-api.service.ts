import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import * as jwtDecode from 'jwt-decode';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { environment } from '../../environments/environment';
import { AuthCredentials, AuthData, AuthUser } from './auth.model';

@Injectable({
  providedIn: 'root'
})
export class AuthApiService {

  constructor(private http: HttpClient) { }

  /**
   * Implementation of authenticate method.
   * It should return already parsed and validated data to auth service.
   * @param credentials authenticate information
   */
  authenticate$(credentials: AuthCredentials): Observable<AuthData> {
    return this.http.post<{ jwt: string; }>(`${environment.apiUriPrefix}/authenticate`, credentials).pipe(
      map(res => {
        // Decode payload
        const payload: {
          id: string;
          username: string;
        } = jwtDecode(res.jwt);
        return {
          ...payload,
          token: res.jwt,
        };
      }),
    );
  }

  /**
   * Converts authenticate data into user.
   * @param data authenticate data
   */
  getUser$(data: AuthData): Observable<AuthUser> {
    return this.http.get<{
      id: string;
      username: string;
      rights: string[];
      avatar: string;
    }>(`${environment.apiUriPrefix}/api/me`, {
      headers: {
        Authorization: data.token
      }
    }).pipe(
      map(res => {
        return {
          ...res,
          token: data.token,
        };
      }),
    );
  }

}
