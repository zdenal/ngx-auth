import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';

import { AuthService } from './auth.service';
import { catchError } from 'rxjs/operators';
import { Router } from '@angular/router';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor(
    protected auth: AuthService,
    protected router: Router,
  ) { }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    // if user is authenticated and authorization header is not present
    if (this.auth.isAuthenticated() && (!request.headers || !request.headers.has('Authorization'))) {
      // set the authorization header
      request = request.clone({ setHeaders: { Authorization: this.auth.getData().token }});
    }
    return next.handle(request).pipe(
      catchError(e => {
        // catch 401 error response, logout and redirect to login page
        if (e instanceof HttpErrorResponse && e.status === 401) {
          this.auth.invalidate();
          this.router.navigate(['login']);
        }
        return throwError(e);
      })
    );
  }

}
