/**
 * Credentials needed for user authorization.
 * They are passed to `AuthServise.authenticate$` function.
 */
export interface AuthCredentials {
  username: string;
  password: string;
}

/**
 * Authorization data stored in client storage.
 * They are synchronous and stored in `AuthService.authData` property.
 */
export interface AuthData {
  id: string;
  username: string;
  token: string;
}

/**
 * Internal representation of user.
 * Usually contains more information than synchronously stored
 * authentication data.
 */
export interface AuthUser extends AuthData {
  rights: string[];
  avatar: string;
}
