import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';

import { AuthUser } from '../auth/auth.model';
import { AuthService } from '../auth/auth.service';
import { Permission, Permissions } from './access.model';

@Injectable({
  providedIn: 'root'
})
export class AccessService {

  private permissions$: Observable<Permissions>;

  constructor(
    private auth: AuthService,
  ) {
    this.permissions$ = this.auth.getUser$().pipe(
      map(user => this.collectPermissions(user)),
      shareReplay(1),
    );
  }

  /**
   * Checks if user has given permission.
   * @param permission tested permission
   */
  hasPermission$(permission: Permission): Observable<boolean> {
    return this.permissions$.pipe(
      map(permissions => !!permissions[permission]),
    );
  }

  /**
   * Collect permissions based on user.
   * @param user authenticated user
   */
  private collectPermissions(user: AuthUser): Permissions {
    if (!user) {
      return {};
    }
    const rights = (user.rights || []).reduce((res, right) => {
      res[right] = true;
      return res;
    }, {});

    // tslint:disable: no-string-literal
    return {
      me: true,
      editor: !!rights['manage'] || !!rights['admin'],
      admin: !!rights['admin'],
    };
  }

}
