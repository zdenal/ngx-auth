/**
 * Object holding boolean permissions.
 */
export interface Permissions {
  me?: boolean;
  editor?: boolean;
  admin?: boolean;
}

/**
 * List of available permissions.
 */
export type Permission = keyof Permissions;
