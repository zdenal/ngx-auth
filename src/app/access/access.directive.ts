import { Directive, Input, OnDestroy, OnInit, TemplateRef, ViewContainerRef } from '@angular/core';
import { BehaviorSubject, Subject } from 'rxjs';
import { filter, switchMap, takeUntil } from 'rxjs/operators';

import { Permission } from './access.model';
import { AccessService } from './access.service';

/**
 * Use this directive to show content based on permissions.
 * To show content use:
 *  <element *appAccess="'admin'"></element>
 */
@Directive({
  selector: '[appAccess]'
})
export class AccessDirective implements OnInit, OnDestroy {

  /**
   * Set to permission to check.
   */
  @Input('appAccess') set value(value: Permission) {
    this.permission$.next(value);
  }

  private permission$ = new BehaviorSubject<Permission>(null);
  private destroyed = new Subject();

  constructor(
    private templateRef: TemplateRef<any>,
    private viewContainer: ViewContainerRef,
    private access: AccessService,
  ) { }

  ngOnInit(): void {
    this.permission$.pipe(
      takeUntil(this.destroyed),
      filter(perm => !!perm),
      switchMap(perm => this.access.hasPermission$(perm)),
    ).subscribe(allow => this.updateView(allow));
  }

  ngOnDestroy() {
    this.destroyed.next();
  }

  protected updateView(visible: boolean): void {
    visible ? this.viewContainer.createEmbeddedView(this.templateRef) : this.viewContainer.clear();
  }

}
