import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, CanActivateChild, Router } from '@angular/router';
import { map } from 'rxjs/operators';

import { AccessService } from './access.service';

@Injectable({
  providedIn: 'root'
})
export class AccessGuard implements CanActivate, CanActivateChild {

  constructor(
    private access: AccessService,
    private router: Router,
  ) {}

  canActivate(route: ActivatedRouteSnapshot) {
    return this.check(route);
  }

  canActivateChild(childRoute: ActivatedRouteSnapshot) {
    return this.check(childRoute);
  }

  private check(route: ActivatedRouteSnapshot) {
    return this.access.hasPermission$(route.data.permission).pipe(
      map(allow => allow || this.router.createUrlTree(['403'])),
    );
  }

}
