import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { AccessDirective } from './access.directive';



@NgModule({
  declarations: [AccessDirective],
  imports: [
    CommonModule
  ],
  exports: [AccessDirective],
})
export class AccessModule { }
