import { Injectable, NgModule } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, CanActivateChild, RouterModule, RouterStateSnapshot } from '@angular/router';

import { AccessGuard } from './access/access.guard';
import { AuthGuard } from './auth/auth.guard';
import { stopAfterFirstNotTrueCanActivateGuard } from './guard/guard.utils';
import { HttpStatusComponent } from './http-status/http-status.component';
import { Permission } from './access/access.model';

/**
 * Guard combining `AuthGuard` and `AccessGuard`.
 */
@Injectable({
  providedIn: 'root'
})
export class AuthAccessGuard implements CanActivate, CanActivateChild {

  constructor(
    private auth: AuthGuard,
    private access: AccessGuard,
  ) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    return stopAfterFirstNotTrueCanActivateGuard(route, state, [this.auth, this.access]);
  }

  canActivateChild(childRoute: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    return stopAfterFirstNotTrueCanActivateGuard(childRoute, state, [this.auth, this.access], true);
  }

}

@NgModule({
  imports: [
    RouterModule.forRoot(
      [
        {
          path: '',
          loadChildren: () => import('./home/home.module').then(m => m.HomeModule),
        },
        {
          path: 'login',
          loadChildren: () => import('./login/login.module').then(m => m.LoginModule),
        },
        {
          path: 'me',
          loadChildren: () => import('./me/me.module').then(m => m.MeModule),
          canActivate: [AuthAccessGuard],
          data: {
            permission: 'me' as Permission
          },
        },
        {
          path: 'editor',
          loadChildren: () => import('./editor/editor.module').then(m => m.EditorModule),
          canActivate: [AuthAccessGuard],
          data: {
            permission: 'editor' as Permission
          },
        },
        {
          path: 'admin',
          loadChildren: () => import('./admin/admin.module').then(m => m.AdminModule),
          canActivate: [AuthAccessGuard],
          data: {
            permission: 'admin' as Permission
          },
        },
        {
          path: '403',
          component: HttpStatusComponent,
          data: {
            status: 403
          }
        },
        {
          path: '**',
          component: HttpStatusComponent,
          data: {
            status: 404
          },
          pathMatch: 'full'
        },
      ]
    )
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
